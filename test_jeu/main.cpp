#include "fonction.hpp"

typedef struct {
    float x = 0, y = 0;
} Position;

typedef struct {
    bool active = false;
    Sprite objet;
    Position position;
} Obstacle;

int main()
{
    int etatDanger = 1;
    int score = 0, vitesse = 10;
    int hauteurEcran = HAUTEUR_ECRAN, largeurEcran = LARGEUR_ECRAN;
    float tpsdeplacement,tpsDanger, tps;
    float rapportEcran = hauteurEcran/ECRAN_REPERE;

    RenderWindow app;//(VideoMode(largeurEcran,hauteurEcran), "SFML window");
  //  Image icon; icon.loadFromFile("icon.png"); app.setIcon(icon.getSize().x, icon.getSize().y, icon.getPixelsPtr());
    app.setFramerateLimit(FPS);

    sf::Time temps;
    Clock clock;
    srand(time(NULL));

    SoundBuffer buffer;                                 //son  pas ouf en vrai c'est chiant
    Sound sound;
    if(!buffer.loadFromFile("ressources/derapage.wav"))
        return EXIT_FAILURE;
    sound.setBuffer(buffer);

    Texture im_bord, im_volant, im_retro, im_retro_danger, im_route, im_aiguille;  //cr�ation Texture et Sprite

    Sprite bord, volant, retro, route, aiguille;

    if (!im_bord.loadFromFile(BORD))
        return EXIT_FAILURE;
    if (!im_volant.loadFromFile(VOLANT))
        return EXIT_FAILURE;
    if (!im_retro.loadFromFile(RETRO))
        return EXIT_FAILURE;
    if (!im_route.loadFromFile(ROUTE))
        return EXIT_FAILURE;
    if (!im_retro_danger.loadFromFile(RETRO_DANGER))
        return EXIT_FAILURE;
    if (!(im_aiguille.loadFromFile(AIGUILLE)))
        return EXIT_FAILURE;

    Texture im_obstacle[NOMBRE_TEXTURES];
    if (!(im_obstacle[0].loadFromFile("ressources/voiture.png")))
        return EXIT_FAILURE;
    if (!(im_obstacle[1].loadFromFile("ressources/glissiere.png")))
        return EXIT_FAILURE;
    if (!(im_obstacle[2].loadFromFile("ressources/poule.png")))
        return EXIT_FAILURE;

    Sprite spriteObstacle[PROBA_OBSTACLE];
    ModePleinEcran(app, Style::Fullscreen, largeurEcran, hauteurEcran, rapportEcran );
    for (int i = 0; i < 3; i++)
    {
        spriteObstacle[i].setTexture(im_obstacle[0]);
        spriteObstacle[i].setScale(rapportEcran,rapportEcran);
        spriteObstacle[i].setOrigin(im_obstacle[0].getSize().x/2,im_obstacle[0].getSize().y);
    }
	for (int i = 3; i < 6; i++)
	{
        spriteObstacle[i].setTexture(im_obstacle[1]);
        spriteObstacle[i].setScale(rapportEcran,rapportEcran);
        spriteObstacle[i].setOrigin(im_obstacle[1].getSize().x/2,im_obstacle[1].getSize().y);
    }
    for (int i = 6; i < 8; i++)
    {
        spriteObstacle[i].setTexture(im_obstacle[2]);
        spriteObstacle[i].setScale(rapportEcran,rapportEcran);
        spriteObstacle[i].setOrigin(im_obstacle[2].getSize().x/2,im_obstacle[2].getSize().y);
    }


    Obstacle obstacle_1, obstacle_2;
    int loop1 = 0, loop2 = 0, route1, route2;

    float deroulement[TAILLE_BASE][2];

    obstacle_1.objet.setScale(rapportEcran,rapportEcran);
    obstacle_2.objet.setScale(rapportEcran,rapportEcran);

    for (int i = 0; i < TAILLE_BASE; i++) {
        int nb1 = nombreAleatoire(1, 3);
        deroulement[i][0] = nb1;
        printf("%i", nb1);

        int nb2 = nombreAleatoire(1, 3);
        while (nb1 == nb2) { nb2 = nombreAleatoire(1, 3); };
        deroulement[i][1] = nb2;
        printf("%i", nb2);

        printf("\n");

    }

    initialisationSprite(bord,im_bord,rapportEcran);
    initialisationSprite(volant,im_volant,rapportEcran);
    initialisationSprite(retro,im_retro_danger,rapportEcran);
    initialisationSprite(route,im_route,rapportEcran);
    initialisationSprite(aiguille,im_aiguille,rapportEcran);
    afficheEcranJeu(app, volant, retro, bord, route, aiguille, largeurEcran, hauteurEcran, rapportEcran);   //dessine ecran jeu

    Event evenement;

    temps = clock.restart();         //debut RTC



    while (app.isOpen())
    {
        while (app.pollEvent(evenement))
        {
            if (evenement.type == Event::Closed)
                app.close();
        }

        if (Keyboard::isKeyPressed(Keyboard::Left) && tpsdeplacement>=0.3)    //touche de gauche
        {
            if(route.getPosition().x < 0)                      //voie disponible
                tournerA(GAUCHE, volant, route, sound, tpsdeplacement, rapportEcran);
            else
                affichageDanger(retro, im_retro_danger, tpsDanger, etatDanger, tpsdeplacement);  //FONCTION DANGER POLICE DANS lE RETRO

        }
        else if (Keyboard::isKeyPressed(Keyboard::Right) && tpsdeplacement>=0.3 )  //touche de droite
        {
            if(route.getPosition().x >=(-LARGEUR_VOIE*rapportEcran))     //voie disponible
                tournerA(DROITE, volant, route, sound, tpsdeplacement, rapportEcran);
            else
                affichageDanger(retro, im_retro_danger, tpsDanger, etatDanger, tpsdeplacement);   //FONCTION DANGER POLICE DANS lE RETRO
        }
        else if (!(Keyboard::isKeyPressed(Keyboard::Left)||Keyboard::isKeyPressed(Keyboard::Right))) //ni droite ni gauche appuy�
        {
            volant.rotate(-volant.getRotation());   //met le volant droit
        }

        if(etatDanger == 2)
        {
            afficheDefaite();
        }
        else if(tpsDanger > 7 && etatDanger == 1)
        {
            affichageNormal(retro, im_retro, etatDanger);
        }

        temps = clock.restart();
        tps += temps.asSeconds();                //tps �coul� depuis le d�but
        tpsdeplacement += temps.asSeconds();     //tps �coul� depuis le dernier mouvement
        tpsDanger += temps.asSeconds();          //tps �coul� depuis etat Danger

        printf("%i : %i\n",route1, route2);

        if (obstacle_1.active)
        {
            obstacle_1.position.y += PAS*rapportEcran * (vitesse/2);
            obstacle_1.position.x = route.getPosition().x +(LARGEUR_VOIE*rapportEcran*(route1))+ (LARGEUR_VOIE*rapportEcran)/2;
        }
        else
        {
            obstacle_1.objet = spriteObstacle[nombreAleatoire(0, PROBA_OBSTACLE-1)];
            obstacle_1.active = true;
            obstacle_1.position.y = 200*rapportEcran;
            route1 = deroulement[loop1][0];
            obstacle_1.position.x = route.getPosition().x +(LARGEUR_VOIE*rapportEcran*(route1)) +(LARGEUR_VOIE*rapportEcran)/2;
            loop1 = (loop1 + 1)%TAILLE_BASE ;
        }

        if (obstacle_2.active) {
            obstacle_2.position.y += PAS*rapportEcran * (vitesse/2);
            obstacle_2.position.x = route.getPosition().x +(LARGEUR_VOIE*rapportEcran*(route2)) + (LARGEUR_VOIE*rapportEcran)/2;
        } else {
            obstacle_2.objet = spriteObstacle[nombreAleatoire(0, PROBA_OBSTACLE-1)];
            obstacle_2.active = true;
            obstacle_2.position.y = 200*rapportEcran;
            route2 = deroulement[loop2][1];
            obstacle_2.position.x = route.getPosition().x + (LARGEUR_VOIE*rapportEcran *(route2)) + (LARGEUR_VOIE*rapportEcran)/2;
            loop2 = (loop2 + 1)%TAILLE_BASE;
        };

        if (obstacle_1.position.y > hauteurEcran)
            obstacle_1.active = false;
        if (obstacle_2.position.y > hauteurEcran)
            obstacle_2.active = false;

        obstacle_1.objet.setPosition(obstacle_1.position.x, obstacle_1.position.y);
        obstacle_2.objet.setPosition(obstacle_2.position.x, obstacle_2.position.y);

        calculEtAfficheScore(tps, score, vitesse, aiguille);

        app.clear();                //clear

        app.draw(route);            //dessine
        app.draw(obstacle_1.objet);
        app.draw(obstacle_2.objet);
        app.draw(bord);
        app.draw(aiguille);
        app.draw(volant);
        app.draw(retro);


        app.display();              //affiche
    }
    return EXIT_SUCCESS;
}
