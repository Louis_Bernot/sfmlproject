#include "fonction.hpp"

void afficheEcranJeu(RenderWindow &app, Sprite &volant, Sprite &retro, Sprite &bord, Sprite &route, Sprite &aiguille, int largeurEcran, int hauteurEcran, float rapportEcran)
{
    volant.setOrigin(volant.getTexture()->getSize().x/2, volant.getTexture()->getSize().y/2);
    volant.setPosition(largeurEcran/2,hauteurEcran*(19/20.0));

    retro.setPosition(posx_retro*rapportEcran, posy_retro*rapportEcran);

    route.setPosition(- (LARGEUR_VOIE*rapportEcran),0);

    aiguille.setOrigin(aiguille.getTexture()->getSize().x/2,aiguille.getTexture()->getSize().y/2);
    aiguille.setPosition(POSX_AIGUILLE*rapportEcran,POSY_AIGUILLE*rapportEcran);
    aiguille.rotate(ROTATION_AIGUILLE);
}

void initialisationSprite(Sprite &sprite, Texture &im_texture, float rapportEcran)
{
    im_texture.setSmooth(true);
    sprite.setTexture(im_texture);
    sprite.setScale(rapportEcran,rapportEcran);
}

void tournerA(int direction, Sprite &volant, Sprite &route, Sound &sound, float &tps, float rapportEcran)
{
    volant.rotate(-volant.getRotation()+ROT_VOLANT*direction);
    route.setPosition(route.getPosition().x-LARGEUR_VOIE*direction*rapportEcran,0);
    sound.play();
    tps = 0;
}

void affichageDanger(Sprite &retro, Texture &im_retro_danger, float &tpsDanger,int &etatDanger, float &tps)
{
    retro.setTexture(im_retro_danger);
    tps = 0;
    tpsDanger = 0;
    etatDanger++;
}

void affichageNormal(Sprite &retro, Texture &im_retro, int &etatDanger)
{
    retro.setTexture(im_retro);
    etatDanger--;
}

void afficheDefaite()
{
    printf("perdu");
}

void calculEtAfficheScore(float &tps, int &score, int &vitesse, Sprite &aiguille)
{
    if(tps>=5 && vitesse != 100)
    {
        vitesse ++;
        tps = 0;
        aiguille.rotate(-aiguille.getRotation()+ROTATION_AIGUILLE+(round((vitesse-10)*2.7)));
        printf("%.2f\n",aiguille.getRotation());
    }
    score += vitesse/10;

//    printf("score : %i\n",score);
}

void ModePleinEcran(RenderWindow &app, int modeEcran, int &largeurEcran, int &hauteurEcran, float &rapportEcran)
{
    app.close();
    largeurEcran = VideoMode::getDesktopMode().width;
    hauteurEcran = VideoMode::getDesktopMode().height;
    rapportEcran = hauteurEcran/ECRAN_REPERE;
    app.create(VideoMode(largeurEcran,hauteurEcran), "Crazy Chicken Driver", modeEcran);
    app.setFramerateLimit(FPS);
}

void ModeEcranReduit(RenderWindow &app, int modeEcran, int &largeurEcran, int &hauteurEcran, float &rapportEcran)
{
    app.close();
    largeurEcran = LARGEUR_ECRAN;
    hauteurEcran = HAUTEUR_ECRAN;
    rapportEcran = hauteurEcran/ECRAN_REPERE;
    app.create(VideoMode(largeurEcran,hauteurEcran), "Crazy Chicken Driver", modeEcran);
    app.setFramerateLimit(FPS);
}

int assigneTouche(int touche,int autreTouche, Event evenement)
{
    int i=1;
    while (i)
    {
        if (Keyboard::isKeyPressed(Keyboard::Escape))
            i = 0;
        else if(evenement.type == Event::KeyPressed)
            return evenement.key.code;
    }
    return touche;
}

int changerVolume(int volumeSon, int bouton)
{
  return volumeSon += bouton;
}

int nombreAleatoire(int min, int max) {
    return rand()%((max+1)-min) + min;
};
