#include <SFML/Graphics.hpp>
#include <SFML/Audio.hpp>
#include <time.h>
#include <math.h>

#define HAUTEUR_ECRAN 450
#define LARGEUR_ECRAN 800
#define posx_retro 1100
#define posy_retro -50
#define POSX_AIGUILLE 293
#define POSY_AIGUILLE 775
#define ROTATION_AIGUILLE -35
#define BORD "ressources/bordv2.png"
#define VOLANT "ressources/volant.png"
#define RETRO "ressources/retro.png"
#define RETRO_DANGER "ressources/retro danger.png"
#define ROUTE "ressources/route.png"
#define AIGUILLE "ressources/aiguille.png"
#define LARGEUR_VOIE 520
#define FPS 60
#define ROT_VOLANT 45

#define ECRAN_REPERE 900.0
#define DROITE 1
#define GAUCHE -1

#define NOMBRE_TEXTURES 3
#define PROBA_OBSTACLE 8
#define TAILLE_BASE 2
#define PAS 1

using namespace sf;

int nombreAleatoire(int min, int max);

void afficheEcranJeu(RenderWindow &app, Sprite &volant, Sprite &retro, Sprite &bord, Sprite &route, Sprite &aiguille, int largeurEcran, int hauteurEcran, float rapportEcran);

void initialisationSprite(Sprite &sprite, Texture &im_texture, float rapportEcran);

void tournerA(int direction, Sprite &volant, Sprite &route, Sound &sound, float &tps, float rapportEcran);

void affichageDanger(Sprite &retro, Texture &im_retro_danger,float &tpsDanger, int &etatDanger, float &tps);

void affichageNormal(Sprite &retro, Texture &im_retro, int &etatDanger);

void afficheDefaite();

void calculEtAfficheScore(float &tps, int &score, int &vitesse, Sprite &aiguille);

void ModePleinEcran(RenderWindow &app, int modeEcran, int &largeurEcran, int &hauteurEcran, float &rapportEcran);

//    ModePleinEcran(app, Style::Fullscreen, largeurEcran, hauteurEcran, rapportEcran );
//    intialisationSprite(bord,im_bord,rapportEcran);
//    intialisationSprite(volant,im_volant,rapportEcran);
//    intialisationSprite(retro,im_retro_danger,rapportEcran);
//    intialisationSprite(route,im_route,rapportEcran);
//    afficheEcranJeu(app, volant, retro, bord, route, largeurEcran, hauteurEcran, rapportEcran);

void ModeEcranReduit(RenderWindow &app, int modeEcran, int &largeurEcran, int &hauteurEcran, float &rapportEcran);

//    ModeEcranReduit(app, Style::Fullscreen, largeurEcran, hauteurEcran, rapportEcran );
//    intialisationSprite(bord,im_bord,rapportEcran);
//    intialisationSprite(volant,im_volant,rapportEcran);
//    intialisationSprite(retro,im_retro_danger,rapportEcran);
//    intialisationSprite(route,im_route,rapportEcran);
//    afficheEcranJeu(app, volant, retro, bord, route, largeurEcran, hauteurEcran, rapportEcran);

int assigneTouche(int touche,int autreTouche, Event evenement);

int changerVolume(int volumeSon, int bouton) ;
