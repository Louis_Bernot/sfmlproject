#include "fonction.hpp"

int main()
{
    int etatDanger = 1;
    int score = 0, vitesse = 10, bonus = 1, record = 0;
    int hauteurEcran = HAUTEUR_ECRAN, largeurEcran = LARGEUR_ECRAN;
    float tpsdeplacement,tpsDanger, tps, tpsbonus;
    float rapportEcran = hauteurEcran/ECRAN_REPERE;
    int volumeSon = SONDEFAUT;
    int Droite = TOUCHEDROITEDEFAUT;
    int Gauche = TOUCHEGAUCHEDEFAUT;
    int jeuEnCours = 1;

    Position fondposition;
    Position sourisposition;
    Texture texturefond;

    RenderWindow app, menu, menuReglage, ecranDefaite;
    //  Image icon; icon.loadFromFile("icon.png"); app.setIcon(icon.getSize().x, icon.getSize().y, icon.getPixelsPtr());

    ModePleinEcran(menu, Style::Fullscreen, largeurEcran, hauteurEcran, rapportEcran);

    sf::Time temps;
    Clock clock;
    srand(time(NULL));

    Font font;
    if (!font.loadFromFile(ARIALTTF))
        return EXIT_FAILURE;
    Text textVolume;
    textVolume.setFont(font);
    char charVolume[5];
    sprintf(charVolume, "%i%s", volumeSon, "%");
    textVolume.setString(charVolume);
    textVolume.setPosition(750*rapportEcran,460*rapportEcran);
    textVolume.setCharacterSize(50*rapportEcran);
    textVolume.setColor(Color::Black);

    Text textScore;
    textScore.setFont(font);
    textScore.setCharacterSize(30*rapportEcran);
    textScore.setColor(Color::Black);
//    Text textGauche;
//    textGauche.setFont(font);
//    char charGauche[5];
//    sprintf(charGauche,"%c",Gauche);
//    textGauche.setString(charGauche);
//    textGauche.setPosition(720*rapportEcran,170*rapportEcran);
//    textGauche.setCharacterSize(50*rapportEcran);
//    textGauche.setColor(Color::Black);

//    Text textDroite;
//    textDroite.setFont(font);
//    char charDroite[5];
//    sprintf(charDroite,"%c",Droite);
//    textDroite.setString(charDroite);
//    textDroite.setPosition(1020*rapportEcran,170*rapportEcran);
//    textDroite.setCharacterSize(50*rapportEcran);
//    textDroite.setColor(Color::Black);

    SoundBuffer buf_derapage;                                 //initialisation des sons
    Sound derapage;
    if(!buf_derapage.loadFromFile(DERAPAGE))
        return EXIT_FAILURE;
    derapage.setBuffer(buf_derapage);

    SoundBuffer buf_poule;
    Sound poule;
    if(!buf_poule.loadFromFile(SONPOULE))
        return EXIT_FAILURE;
    poule.setBuffer(buf_poule);

    SoundBuffer buf_braquage;
    Sound braquage;
    if(!buf_braquage.loadFromFile(BRAQUAGE))
        return EXIT_FAILURE;
    braquage.setBuffer(buf_braquage);

    SoundBuffer buf_Sirenepolice;
    Sound Sirenepolice;
    if(!buf_Sirenepolice.loadFromFile(SIRENE))
        return EXIT_FAILURE;
    Sirenepolice.setBuffer(buf_Sirenepolice);

    SoundBuffer buf_defaite;
    Sound defaite;
    if(!buf_defaite.loadFromFile(DEFAITE))
        return EXIT_FAILURE;
    defaite.setBuffer(buf_defaite);

    SoundBuffer buf_accident;
    Sound accident;
    if(!buf_accident.loadFromFile(ACCIDENT))
        return EXIT_FAILURE;
    accident.setBuffer(buf_accident);

    SoundBuffer buf_choc;
    Sound choc;
    if(!buf_choc.loadFromFile(CHOC))
        return EXIT_FAILURE;
    choc.setBuffer(buf_choc);

    setVolume( derapage, poule, Sirenepolice, defaite, braquage, accident, choc, volumeSon);

    Texture im_bord, im_volant, im_retro, im_retro_danger, im_route, im_aiguille, im_menu, im_menuReglage, im_ecranfin;  //cr�ation Texture et Sprite

    Sprite bord, volant, retro, route, aiguille, fondmenus;

    if (!im_bord.loadFromFile(BORD))
        return EXIT_FAILURE;
    if (!im_volant.loadFromFile(VOLANT))
        return EXIT_FAILURE;
    if (!im_retro.loadFromFile(RETRO))
        return EXIT_FAILURE;
    if (!im_route.loadFromFile(ROUTE))
        return EXIT_FAILURE;
    if (!im_retro_danger.loadFromFile(RETRO_DANGER))
        return EXIT_FAILURE;
    if (!(im_aiguille.loadFromFile(AIGUILLE)))
        return EXIT_FAILURE;
    if (!im_menu.loadFromFile(FOND))
        return EXIT_FAILURE;
    if (!im_menuReglage.loadFromFile(FONDREGLAGE))
        return EXIT_FAILURE;
    if(!im_ecranfin.loadFromFile(ECRANFIN))
        return EXIT_FAILURE;

    Texture im_obstacle[NOMBRE_TEXTURES];
    if (!(im_obstacle[0].loadFromFile(VOITURE)))
        return EXIT_FAILURE;
    if (!(im_obstacle[1].loadFromFile(GLISSIERE)))
        return EXIT_FAILURE;
    if (!(im_obstacle[2].loadFromFile(POULE)))
        return EXIT_FAILURE;

    Sprite spriteObstacle[PROBA_OBSTACLE];
//    ModePleinEcran(app, Style::Fullscreen, largeurEcran, hauteurEcran, rapportEcran );
    for (int i = 0; i < 3; i++)
    {
        initialisationSprite(spriteObstacle[i], im_obstacle[0], rapportEcran);
        spriteObstacle[i].setOrigin(im_obstacle[0].getSize().x/2,im_obstacle[0].getSize().y);
    }
    for (int i = 3; i < 6; i++)
    {
        initialisationSprite(spriteObstacle[i], im_obstacle[1], rapportEcran);
        spriteObstacle[i].setOrigin(im_obstacle[1].getSize().x/2,im_obstacle[1].getSize().y);
    }
    for (int i = 6; i < 8; i++)
    {
        initialisationSprite(spriteObstacle[i], im_obstacle[2], rapportEcran);
        spriteObstacle[i].setOrigin(im_obstacle[2].getSize().x/2,im_obstacle[2].getSize().y);
    }

    Obstacle obstacle_1, obstacle_2;
    int loop1 = 0, loop2 = 0, route1, route2;

    float deroulement[TAILLE_BASE][2];

    obstacle_1.objet.setScale(rapportEcran,rapportEcran);
    obstacle_2.objet.setScale(rapportEcran,rapportEcran);

    for (int i = 0; i < TAILLE_BASE; i++)
    {
        int nb1 = nombreAleatoire(1, 3);
        deroulement[i][0] = nb1;
        printf("%i", nb1);

        int nb2 = nombreAleatoire(1, 3);
        while (nb1 == nb2)
        {
            nb2 = nombreAleatoire(1, 3);
        };
        deroulement[i][1] = nb2;
        printf("%i", nb2);

        printf("\n");

    }

    initialisationSprite(bord,im_bord,rapportEcran);
    initialisationSprite(volant,im_volant,rapportEcran);
    initialisationSprite(retro,im_retro_danger,rapportEcran);
    initialisationSprite(route,im_route,rapportEcran);
    initialisationSprite(aiguille,im_aiguille,rapportEcran);
    initialisationSprite(fondmenus, im_menu, rapportEcran);
    afficheEcranJeu(app, volant, retro, bord, route, aiguille, largeurEcran, hauteurEcran, rapportEcran);   //dessine ecran jeu

    Event evenement;


    while(jeuEnCours)
    {
        obstacle_1.active = false;
        obstacle_2.active = false;
        etatDanger = 1;
        score = 0;
        route.setPosition(- (LARGEUR_VOIE*rapportEcran),0);
        vitesse = 10;
        while(menu.isOpen())
        {
            while(menu.pollEvent(evenement))
            {
                Vector2i sourisposition = Mouse::getPosition(menu);

                if (Mouse::isButtonPressed(Mouse::Left))
                {
                    if     (sourisBienPlace(sourisposition, 300, 644, 344, 440, rapportEcran))
                    {
                        //JOUER
                        printf("jouer\n");
                        ModePleinEcran(app, Style::Fullscreen, largeurEcran, hauteurEcran, rapportEcran);
                        menu.close();
                    }
                    else if(sourisBienPlace(sourisposition, 300, 644, 502, 598, rapportEcran))
                    {
                        //R�GLAGES
                        initialisationSprite(fondmenus, im_menuReglage, rapportEcran);
                        ModePleinEcran(menuReglage, Style::Fullscreen, largeurEcran, hauteurEcran, rapportEcran);
                        menu.close();

                        while (menuReglage.isOpen())
                        {
                            while (menuReglage.pollEvent(evenement))
                            {
                                Vector2i sourisposition = Mouse::getPosition(menuReglage);
                                //printf("%i-%i\n",sourisposition.x,sourisposition.y);

                                if (Mouse::isButtonPressed(Mouse::Left))
                                {
                                    /*                 if     (sourisposition.x > 512*rapportEcran && sourisposition.x < 765*rapportEcran &&
                                                             sourisposition.y > 141*rapportEcran && sourisposition.y < 256*rapportEcran)
                                                     {
                                                         //GAUCHE
                                                         //printf("gauche\n");
                                                         Gauche = assigneTouche(Gauche,Droite,menuReglage);
                                    //                                    sprintf(charGauche,"%c",Gauche);
                                    //                                    textGauche.setString(charGauche);
                                                     }
                                                     else if(sourisposition.x > 824*rapportEcran && sourisposition.x < 1077*rapportEcran &&
                                                             sourisposition.y > 141*rapportEcran && sourisposition.y < 256*rapportEcran)
                                                     {
                                                         //DROITE
                                                         //printf("droite\n");
                                                         Droite = assigneTouche(Droite,Gauche,menuReglage);
                                    //                                    sprintf(charDroite,"%c",Droite);
                                    //                                    textDroite.setString(charDroite);
                                                     }
                                                     else*/ if(sourisBienPlace(sourisposition, 1140, 1466, 126, 266, rapportEcran))
                                    {
                                        //PAR D�FAUT TOUCHES
                                        //printf("par defaut touches\n");
                                        touchespardefaut (Droite,Gauche);
                                    }
                                    else if(sourisBienPlace(sourisposition, 538, 653, 435, 550, rapportEcran))
                                    {
                                        //-
                                        //printf("-\n");
                                        volumeSon = (volumeSon+101-1)%101;

                                        setVolume( derapage, poule, Sirenepolice, defaite, braquage, accident, choc, volumeSon);

                                        sprintf(charVolume, "%i%s", volumeSon, "%");
                                        textVolume.setString(charVolume);
                                    }
                                    else if(sourisBienPlace(sourisposition, 938, 1053, 435, 550, rapportEcran))
                                    {
                                        //+
                                        //printf("+\n");
                                        volumeSon = (volumeSon+101+1)%101;

                                        setVolume( derapage, poule, Sirenepolice, defaite, braquage, accident, choc, volumeSon);

                                        sprintf(charVolume, "%i%s", volumeSon, "%");
                                        textVolume.setString(charVolume);
                                    }
                                    else if(sourisBienPlace(sourisposition, 1140, 1466, 420, 560, rapportEcran))
                                    {
                                        //PAR D�FAUT SON
                                        //printf("par defaut son\n");
                                        sonpardefaut (volumeSon);

                                        setVolume( derapage, poule, Sirenepolice, defaite, braquage, accident, choc, volumeSon);

                                        sprintf(charVolume, "%i%s", volumeSon, "%");
                                        textVolume.setString(charVolume);
                                    }
                                    else if(sourisBienPlace(sourisposition, 562, 1026, 688, 805, rapportEcran))
                                    {
                                        //QUITTER
                                        menuReglage.close();
                                        ModePleinEcran(menu, Style::Fullscreen, largeurEcran, hauteurEcran, rapportEcran);
                                        fondmenus.setTexture(im_menu);
                                    }
                                }
                            }
                            menuReglage.clear();
                            menuReglage.draw(fondmenus);
//                        menuReglage.draw(textGauche);
//                        menuReglage.draw(textDroite);
                            menuReglage.draw(textVolume);
                            menuReglage.display();
                        }

                    }
                    else if(sourisBienPlace(sourisposition, 300, 644, 662, 758, rapportEcran))
                    {
                        //QUITTER
                        menu.close();
                        jeuEnCours = 0;
                    }
                }
            }
            menu.clear();
            menu.draw(fondmenus);
            scoreMenu(record, menu, textScore, rapportEcran);
            menu.display();
        }

        if(jeuEnCours)
        {
            app.display();
            app.clear(Color::Black);

            braquage.play();
            while(braquage.getStatus())
            {}

            Sirenepolice.play();

            temps = clock.restart();         //debut RTC
        }

        while (app.isOpen())    //fenetre de jeu
        {
            while (app.pollEvent(evenement))
            {
                if (evenement.type == Event::Closed)
                    app.close();
            }

            if (Keyboard::isKeyPressed(Keyboard::Left) && tpsdeplacement>=0.3)    //touche de gauche
            {
                if(route.getPosition().x < 0)                      //voie disponible
                    tournerA(GAUCHE, volant, route, derapage, tpsdeplacement, rapportEcran);
                else
                {
                    affichageDanger(retro, im_retro_danger, tpsDanger, etatDanger, tpsdeplacement);  //FONCTION DANGER POLICE DANS lE RETRO
                    choc.play();
                    Sirenepolice.play();
                }

            }
            else if (Keyboard::isKeyPressed(Keyboard::Right) && tpsdeplacement>=0.3 )  //touche de droite
            {
                if(route.getPosition().x >=(-LARGEUR_VOIE*rapportEcran))     //voie disponible
                    tournerA(DROITE, volant, route, derapage, tpsdeplacement, rapportEcran);
                else
                {
                    affichageDanger(retro, im_retro_danger, tpsDanger, etatDanger, tpsdeplacement);   //FONCTION DANGER POLICE DANS lE RETRO
                    choc.play();
                    Sirenepolice.play();
                }
            }
            else if (!(Keyboard::isKeyPressed(Keyboard::Left)||Keyboard::isKeyPressed(Keyboard::Right))) //ni droite ni gauche appuy�
            {
                volant.rotate(-volant.getRotation());   //met le volant droit
            }

            if(etatDanger == 2)
            {
                afficheDefaite(Sirenepolice);
                accident.play();
                while (accident.getStatus());
                defaite.play();
                ModePleinEcran(ecranDefaite, Style::Fullscreen, largeurEcran, hauteurEcran, rapportEcran);
                app.close();
            }
            else if(tpsDanger > 7 && etatDanger == 1)
            {
                affichageNormal(retro, im_retro, etatDanger);
            }

            printf("%i : %i\n",route1, route2);

            if (obstacle_1.active)  //sinon l'obstacle 1 existe
            {
                obstacle_1.position = deplacementObstacle(obstacle_1.position, vitesse, route, route1, rapportEcran);   //deplace un obstacle
            }
            else
            {
                int proba = nombreAleatoire(0, PROBA_OBSTACLE-1);
                creationObstacle(obstacle_1, spriteObstacle[proba], rapportEcran, loop1, proba);       //sinon il le cr�er
                route1 = deroulement[loop1][0];
                obstacle_1.position = deplacementObstacle(obstacle_1.position, vitesse, route, route1, rapportEcran);
            }

            if (obstacle_2.active)
            {
                obstacle_2.position = deplacementObstacle(obstacle_2.position, vitesse, route, route2, rapportEcran);
            }
            else
            {
                int proba = nombreAleatoire(0, PROBA_OBSTACLE-1);
                creationObstacle(obstacle_2, spriteObstacle[proba], rapportEcran, loop2, proba);
                route2 = deroulement[loop2][1];
                obstacle_2.position = deplacementObstacle(obstacle_2.position, vitesse, route, route2, rapportEcran);
            }

            if (obstacle_1.position.y > hauteurEcran)
                obstacle_1.active = false;
            if (obstacle_2.position.y > hauteurEcran)
                obstacle_2.active = false;

            obstacle_1.objet.setPosition(obstacle_1.position.x, obstacle_1.position.y);
            obstacle_2.objet.setPosition(obstacle_2.position.x, obstacle_2.position.y);

            if (!obstacle_1.collision && (obstacle_1.position.y > 615*rapportEcran) && (obstacle_1.position.y <= 800*rapportEcran) && (obstacle_1.position.x/rapportEcran >= 780) && (obstacle_1.position.x/rapportEcran <= 781))
            {
                obstacle_1.collision = true;
                if (obstacle_1.cool)
                {
                    bonus = 2;
                    poule.play();
                }
                else
                {
                    affichageDanger(retro, im_retro_danger, tpsDanger, etatDanger, tpsdeplacement);
                    choc.play();
                    Sirenepolice.play();
                }

            }
            else if (!obstacle_2.collision && (obstacle_2.position.y > 615*rapportEcran) && (obstacle_1.position.y <= 800*rapportEcran) && (obstacle_2.position.x/rapportEcran >= 780) && (obstacle_2.position.x/rapportEcran <= 781))
            {
                obstacle_2.collision = true;
                if (obstacle_2.cool)
                {
                    bonus = 2;
                    poule.play();
                }
                else
                {
                    affichageDanger(retro, im_retro_danger, tpsDanger, etatDanger, tpsdeplacement);
                    choc.play();
                    Sirenepolice.play();
                }
            }

            if (tpsbonus > 2)
                bonus = 1;

            temps = clock.restart();
            tps += temps.asSeconds();                //tps �coul� depuis le d�but
            tpsdeplacement += temps.asSeconds();     //tps �coul� depuis le dernier mouvement
            tpsDanger += temps.asSeconds();          //tps �coul� depuis etat Danger
            tpsbonus += temps.asSeconds();

            app.clear();                //clear

            app.draw(route);            //dessine
            calculEtAfficheScore(tps, score, vitesse, aiguille, bonus, app, textScore, rapportEcran);
            app.draw(obstacle_1.objet);
            app.draw(obstacle_2.objet);
            app.draw(bord);
            app.draw(aiguille);
            app.draw(volant);
            app.draw(retro);

            app.display();              //affiche
        }
        while(ecranDefaite.isOpen())
        {
            initialisationSprite(fondmenus, im_ecranfin, rapportEcran);
            while (ecranDefaite.pollEvent(evenement))
            {
                Vector2i sourisposition = Mouse::getPosition(ecranDefaite);
                if (Mouse::isButtonPressed(Mouse::Left))
                {
                    if(sourisBienPlace(sourisposition, 296, 646, 536, 632, rapportEcran))
                    {
                        //QUITTER
                        ecranDefaite.close();
                        ModePleinEcran(menu, Style::Fullscreen, largeurEcran, hauteurEcran, rapportEcran);
                        fondmenus.setTexture(im_menu);
                        sleep(milliseconds(500));
                        if (record<score)
                            record = score;
                    }
                }
                ecranDefaite.clear();
                ecranDefaite.draw(fondmenus);
                scoreMenu( score, ecranDefaite, textScore, rapportEcran);
                ecranDefaite.display();
            }
        }
    }
    return EXIT_SUCCESS;
}
