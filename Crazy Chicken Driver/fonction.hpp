#include <SFML/Graphics.hpp>
#include <SFML/Audio.hpp>
#include <time.h>
#include <math.h>

#define HAUTEUR_ECRAN 450
#define LARGEUR_ECRAN 800
#define posx_retro 1100
#define posy_retro -50
#define POSX_AIGUILLE 293
#define POSY_AIGUILLE 775
#define ROTATION_AIGUILLE -35
#define BORD "ressources/bord.png"
#define VOLANT "ressources/volant.png"
#define RETRO "ressources/retro.png"
#define RETRO_DANGER "ressources/retro danger.png"
#define ROUTE "ressources/route.png"
#define AIGUILLE "ressources/aiguille.png"
#define VOITURE "ressources/voiture.png"
#define GLISSIERE "ressources/glissiere.png"
#define POULE "ressources/poule.png"
#define FOND "ressources/fond.png"
#define FONDREGLAGE "ressources/fondreglages.jpg"
#define ECRANFIN "ressources/ecrandefin.png"
#define LARGEUR_VOIE 520
#define FPS 60
#define ROT_VOLANT 45
#define TOUCHEDROITEDEFAUT Keyboard::Right
#define TOUCHEGAUCHEDEFAUT Keyboard::Left
#define SONDEFAUT 20
#define ARIALTTF "ressources/arial.ttf"
#define DERAPAGE "ressources/derapage.wav"
#define SONPOULE "ressources/poule.wav"
#define BRAQUAGE "ressources/braquage.wav"
#define SIRENE "ressources/Sirenepolice.wav"
#define DEFAITE "ressources/defaite.wav"
#define ACCIDENT "ressources/accident.wav"
#define CHOC "ressources/choc.wav"

#define ECRAN_REPERE 900.0
#define DROITE 1
#define GAUCHE -1

#define NOMBRE_TEXTURES 3
#define PROBA_OBSTACLE 8
#define TAILLE_BASE 50
#define PAS 1

using namespace sf;

typedef struct {
    float x = 0, y = 0;
} Position;

typedef struct {
    bool active = false, collision = false, cool;
    Sprite objet;
    Position position;
} Obstacle;

int nombreAleatoire(int min, int max);

void afficheEcranJeu(RenderWindow &app, Sprite &volant, Sprite &retro, Sprite &bord, Sprite &route, Sprite &aiguille, int largeurEcran, int hauteurEcran, float rapportEcran);

void initialisationSprite(Sprite &sprite, Texture &im_texture, float rapportEcran);

void tournerA(int direction, Sprite &volant, Sprite &route, Sound &sound, float &tps, float rapportEcran);

void affichageDanger(Sprite &retro, Texture &im_retro_danger,float &tpsDanger, int &etatDanger, float &tps);

void affichageNormal(Sprite &retro, Texture &im_retro, int &etatDanger);

void afficheDefaite(Sound &Sirenepolice);

void calculEtAfficheScore(float &tps, int &score, int &vitesse, Sprite &aiguille, int bonus, RenderWindow &fenetre, Text &text, float rapportEcran);

void ModePleinEcran(RenderWindow &app, int modeEcran, int &largeurEcran, int &hauteurEcran, float &rapportEcran);

void ModeEcranReduit(RenderWindow &app, int modeEcran, int &largeurEcran, int &hauteurEcran, float &rapportEcran);

int assigneTouche(int touche,int autreTouche, RenderWindow &app);

void touchespardefaut (int &droite, int &gauche);

int changerVolume(int &volumeSon, int bouton) ;

void sonpardefaut (int &volume);

Position deplacementObstacle(Position position, int vitesse, Sprite route, int route2, float rapportEcran);

void creationObstacle(Obstacle &obstacle, Sprite spriteObstacle, float rapportEcran, int &loop, int proba);

void setVolume (Sound &derapage, Sound &poule, Sound &Sirenepolice, Sound &defaite, Sound &braquage, Sound &accident, Sound &choc, int volumeSon);

void scoreMenu(int score, RenderWindow &fenetre , Text &text, float rapportEcran);

bool sourisBienPlace(Vector2i sourisposition, int xMin, int xMax, int yMin, int yMax, float rapportEcran);
