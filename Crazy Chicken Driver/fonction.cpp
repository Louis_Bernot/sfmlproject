#include "fonction.hpp"

void afficheEcranJeu(RenderWindow &app, Sprite &volant, Sprite &retro, Sprite &bord, Sprite &route, Sprite &aiguille, int largeurEcran, int hauteurEcran, float rapportEcran)
{
    volant.setOrigin(volant.getTexture()->getSize().x/2, volant.getTexture()->getSize().y/2);
    volant.setPosition(largeurEcran/2,hauteurEcran*(19/20.0));

    retro.setPosition(posx_retro*rapportEcran, posy_retro*rapportEcran);

    route.setPosition(- (LARGEUR_VOIE*rapportEcran),0);

    aiguille.setOrigin(aiguille.getTexture()->getSize().x/2,aiguille.getTexture()->getSize().y/2);
    aiguille.setPosition(POSX_AIGUILLE*rapportEcran,POSY_AIGUILLE*rapportEcran);
    aiguille.rotate(ROTATION_AIGUILLE);
}

void initialisationSprite(Sprite &sprite, Texture &im_texture, float rapportEcran)
{
    im_texture.setSmooth(true);
    sprite.setTexture(im_texture);
    sprite.setScale(rapportEcran,rapportEcran);
}

void tournerA(int direction, Sprite &volant, Sprite &route, Sound &derapage, float &tps, float rapportEcran)
{
    volant.rotate(-volant.getRotation()+ROT_VOLANT*direction);
    route.setPosition(route.getPosition().x-LARGEUR_VOIE*direction*rapportEcran,0);
    derapage.play();
    tps = 0;
}

void affichageDanger(Sprite &retro, Texture &im_retro_danger, float &tpsDanger,int &etatDanger, float &tps)
{
    retro.setTexture(im_retro_danger);
    tps = 0;
    tpsDanger = 0;
    etatDanger++;
}

void affichageNormal(Sprite &retro, Texture &im_retro, int &etatDanger)
{
    retro.setTexture(im_retro);
    etatDanger--;
}

void afficheDefaite(Sound &Sirenepolice)
{
    Sirenepolice.stop();
    printf("perdu");
}

void calculEtAfficheScore(float &tps, int &score, int &vitesse, Sprite &aiguille, int bonus, RenderWindow &fenetre, Text &text, float rapportEcran)
{
    if(tps>=5 && vitesse != 100)
    {
        vitesse ++;
        tps = 0;
        aiguille.rotate(-aiguille.getRotation()+ROTATION_AIGUILLE+(round((vitesse-10)*2.7)));
        printf("%.2f\n",aiguille.getRotation());
    }
    score += (vitesse/10)*bonus;

    char textScore[30];
    sprintf(textScore, "%i%s", score, "PTS");
    text.setString(textScore);
    text.setPosition(10* rapportEcran,10* rapportEcran);
    fenetre.draw(text);

//    printf("score : %i\n",score);
}

void ModePleinEcran(RenderWindow &app, int modeEcran, int &largeurEcran, int &hauteurEcran, float &rapportEcran)
{
    app.close();
    largeurEcran = VideoMode::getDesktopMode().width;
    hauteurEcran = VideoMode::getDesktopMode().height;
    rapportEcran = hauteurEcran/ECRAN_REPERE;
    app.create(VideoMode(largeurEcran,hauteurEcran), "Crazy Chicken Driver", modeEcran);
    app.setFramerateLimit(FPS);
}

void ModeEcranReduit(RenderWindow &app, int modeEcran, int &largeurEcran, int &hauteurEcran, float &rapportEcran)
{
    app.close();
    largeurEcran = LARGEUR_ECRAN;
    hauteurEcran = HAUTEUR_ECRAN;
    rapportEcran = hauteurEcran/ECRAN_REPERE;
    app.create(VideoMode(largeurEcran,hauteurEcran), "Crazy Chicken Driver", modeEcran);
    app.setFramerateLimit(FPS);
}

int assigneTouche(int touche,int autreTouche, RenderWindow &app)
{
    Event evenement;
    int i=1;
    while (i)
    {
        while (app.pollEvent(evenement))
        {
            if (evenement.type == Event::KeyPressed)
            {
                if (evenement.key.code == sf::Keyboard::Escape)
                    i = 0;
                else if(evenement.key.code != autreTouche)
                {
                    //printf("%i",evenement.key.code);
                    touche = evenement.key.code;
                    i = 0;
                }
            }
        }
    }
    return touche;
}

void touchespardefaut (int &droite, int &gauche)
{
    droite = TOUCHEDROITEDEFAUT;
    gauche = TOUCHEGAUCHEDEFAUT;
}

void sonpardefaut (int &volume)
{
    volume = SONDEFAUT;
}

int nombreAleatoire(int min, int max) {
    return rand()%((max+1)-min) + min;
};

Position deplacementObstacle(Position position, int vitesse, Sprite route, int voie, float rapportEcran)
{
    position.y += PAS*rapportEcran * (vitesse/3);
    position.x = route.getPosition().x +(LARGEUR_VOIE*rapportEcran*(voie)) + (LARGEUR_VOIE*rapportEcran)/2;
    return position;
}

void creationObstacle(Obstacle &obstacle, Sprite spriteObstacle, float rapportEcran, int &loop, int proba)
{
    obstacle.objet = spriteObstacle;
    obstacle.active = true;
    obstacle.collision = false;
    obstacle.position.y = 200*rapportEcran;
        loop = (loop + 1)%TAILLE_BASE;
                if (proba == 6 || proba == 7)
                    obstacle.cool = true;
                else
                    obstacle.cool = false;
}

void setVolume (Sound &derapage, Sound &poule, Sound &Sirenepolice, Sound &defaite, Sound &braquage, Sound &accident, Sound &choc, int volumeSon)
{
    derapage.setVolume(volumeSon*0.2);
    poule.setVolume(volumeSon);
    Sirenepolice.setVolume(volumeSon*0.7);
    defaite.setVolume(volumeSon);
    braquage.setVolume(volumeSon);
    choc.setVolume(volumeSon);
}

void scoreMenu(int score, RenderWindow &fenetre , Text &text, float rapportEcran)
{
    char textScore[30];
    sprintf(textScore, "%i%s", score, "PTS");
    text.setString(textScore);
    text.setPosition(1200* rapportEcran,705* rapportEcran);
    fenetre.draw(text);
}

bool sourisBienPlace(Vector2i sourisposition, int xMin, int xMax, int yMin, int yMax, float rapportEcran)
{
    return (sourisposition.x > xMin*rapportEcran && sourisposition.x < xMax*rapportEcran &&
           sourisposition.y > yMin*rapportEcran && sourisposition.y < yMax*rapportEcran );
}
