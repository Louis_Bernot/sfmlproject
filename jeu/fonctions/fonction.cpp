#include <SFML/Graphics.hpp>
#include "fonction.hpp"
using namespace sf;
void tableau_bord(RenderWindow &app, Sprite volant, Sprite retro, Sprite bord, Texture im_volant)
{
    volant.setOrigin(im_volant.getSize().x/2, im_volant.getSize().x/2);
    volant.setPosition(posx_centre_volant/2, posy_centre_volant/2);

    retro.setPosition(posx_retro/2, posy_retro/2);

    bord.setScale(0.5,0.5);
    volant.setScale(0.5,0.5);
    retro.setScale(0.5,0.5);

    app.draw(bord);
    app.draw(volant);
    app.draw(retro);
    app.display();
}