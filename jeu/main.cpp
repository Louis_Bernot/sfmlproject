#include <SFML/Graphics.hpp>
#include "ressources/fonction.hpp"
#define HAUTEUR_ECRAN 450
#define LARGEUR_ECRAN 800
#define route "ressources/route_test.png"
#define taille_voie 520/2
#define POS_INIT_ROUTE_X 500/2
#define FPS 60
using namespace sf;

int main()
{ 
    RenderWindow app(VideoMode(LARGEUR_ECRAN,HAUTEUR_ECRAN), "SFML window");// Create the main window
    app.setFramerateLimit(FPS);
    
    Texture texture, im_voiture, im_volant, im_retro;// Load a sprite to display
    if (!texture.loadFromFile(route))
        return EXIT_FAILURE;
    if (!im_voiture.loadFromFile(BORD))
        return EXIT_FAILURE;
    if (!im_volant.loadFromFile(VOLANT))
        return EXIT_FAILURE;
    if (!im_retro.loadFromFile(RETRO))
        return EXIT_FAILURE;
    Sprite sprite(texture); Sprite bord (im_voiture);
    Sprite volant (im_volant); Sprite retro (im_retro);
    sprite.setScale(0.5,0.5);//
    sprite.setPosition(- POS_INIT_ROUTE_X,0);//
    
    tableau_bord(app, volant, retro, bord, im_volant);
    
    while (app.isOpen())// Start the game loop
    {
        Event evenement;// Process events
        while (app.pollEvent(evenement))
        {
            if (eevenementvent.type == Event::KeyPressed)
            {
                if(evenement.key.code == Keyboard::Escape) // Close window : exit
                    app.close();
                if (evenement.key.code == Keyboard::Left)
                    volant.rotate(-volant.getRotation()-ROT_VOLANT);
                else if (evenement.key.code == Keyboard::Right)
                    volant.rotate(-volant.getRotation()+ROT_VOLANT);
            }
            if (evenement.type == Event::KeyReleased)
            {
                if((evenement.key.code == Keyboard::Right) && (sprite.getPosition().x >=(-POS_INIT_ROUTE_X)))
                {
                    sprite.setPosition(sprite.getPosition().x-taille_voie,0);
                    volant.rotate(-volant.getRotation());
                }
                if(evenement.key.code == Keyboard::Left && (sprite.getPosition().x < 0))
                {
                    sprite.setPosition(sprite.getPosition().x+taille_voie,0);
                    volant.rotate(-volant.getRotation());
                }       
            }
        }
        app.clear(); // Clear screen
        app.draw(sprite);   // Draw the sprite//
        app.display();// Update the window//
    }
    return EXIT_SUCCESS;
}